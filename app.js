const express = require('express')
const app = express()
const port = 1234
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const bodyParser = require('body-parser')
const adapter = new FileSync('db.json')
const db = low(adapter)
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

const getRules = () => {
  const r = 'rules'

  if (!db.has(r).value()) {
    throw 'db has no rules'
  }

  return db.get(r).value()
}

const validation = (params) => {
  const rules = getRules()
  const eliglibleMessage = 'This product is eliglible.'
  const notEligibleMessage = 'Sometime soon. This product isn\'t eliglible for our new shippig program yet.'

  for (const name in params) {
    // check to see if fields posted have rules
    if (!rules.hasOwnProperty(name)) {
      return `The '${name}' field doesn't have any associated rules. Double-check your request.`
    }
    const set = rules[name]
    for (const prop in set) {

      // validation rules
      if (prop === 'includes') {
        if (!set[prop].includes(params[name])) {
          return notEligibleMessage
        }
      }

      if (prop === 'greaterThanOrEqual') {
        if (set[prop] > parseInt(params[name], 10)) {
          return notEligibleMessage
        }
      }
      // more rules can be added here
      // if (prop === 'lessThanOrEqual') {}
      // if (prop === 'excludes') {}

    }
  }
  return eliglibleMessage
}

console.log(getRules())

app.post('/tryItem', (req, res) => {
  const response = validation(req.body)
  res.json({ response })
})

// easy to expose simple api to write and read rules
app.get('/getRules', (req, res) => {
  res.json(getRules())
})

app.post('/setRules', (req, res) => {
  db.set('rules', req.body).write()
  res.json(getRules())
})

app.listen(port, () => console.log(`api server started on port ${port}`))
