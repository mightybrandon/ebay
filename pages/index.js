import React, { useState, useRef } from 'react'
import Head from 'next/head'
import { ClientForm } from './client_form'

import './styles.less'

const API_URL = 'http://localhost:1234/tryItem'

function Index() {
  const [eligible, setEligible] = useState('')

  async function handleSubmit(e) {
    e.preventDefault()
    const t = e.target
    const title = t.title.value
    const price = t.price.value
    const category = t.category.value
    const username = t.username.value

    const data = { title, price, category, username }

    let response = await fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    })

    response = await response.json()
    return setEligible(response.response)
  }

  return (
    <div className="container">
      <Head>
        <title>Brandon's Ebay API</title>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className="logo">
          <img src="/logo.png" />
        </div>
        <div className="notification">{eligible}</div>
        <div className="logo__container">
          Let's check to see if your item is eligible for our new shipping
          program.
        </div>
        <ClientForm handleSubmit={handleSubmit} />
      </main>
      <footer></footer>
    </div>
  )
}

export default Index
