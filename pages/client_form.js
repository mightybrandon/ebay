import React, { useState, useRef } from 'react'

export function ClientForm(props) {
  return (
    <form onSubmit={props.handleSubmit}>
      <input type="text" name="title" placeholder="What do you call it?" required />
      <input type="text" name="username" placeholder="What's the seller's username?" required />
      <input type="number" name="category" placeholder="What's the item's category number?" required />
      <input type="text" name="price" placeholder="How much does the item cost?" required />
      <input type="submit" />
    </form>
  );
}