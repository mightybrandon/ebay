Thanks for checking this out.

Tried to keep this as simple as I could. Could easily to put 30+ hours into this. 

![alt text](https://i.imgur.com/u6avSkd.jpg)


1. npm install
2. npm run api
3. npm run dev

### POST - /tryItem - check items eligibility
### GET - /getRules - see current rules to validate for
### POST - /setRules - set rules to validate for


db.json has some rule definitions



Things missing:
* unit tests
* api edge cases
* front end formatting / api response

branthorn007@gmail.com